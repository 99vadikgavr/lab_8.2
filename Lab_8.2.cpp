﻿/*Дана целочисленная матрица {Aij}i=1..n,j=1..m (n,m<=100).
Найти строку, сумма элементов которой наиболее близка к 0, и заменить все элементы этой строки числом 0.

Переделайте задание #3 из лабораторной работы #4 своего варианта, разместив массивы в динамической
памяти. Размеры массивов задайте согласно введенным данным.*/

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");

    ifstream fin("infile.txt");
    ofstream fout("outfile.txt");

    int I, J, x;
    fin >> I;
    fin >> J;

    int n;

    if (I > 100 || I <= 0 || J > 100 || J <= 0)
    {
        cout << "Введено число неудовлетворительное условию: (n,m<=100)." << endl;
        return 1;
    }

    if (I == 1 && J == 1)
    {
        fin >> n;
        fout << n;
        
        cout << "Введена матрица 1 х 1." << endl;
        return 3;
    }

    int **matr = new int*[I];

    int Length_I; //Переменная для нахождения кол-ва столбцов матрицы
    int Length_J; //Переменная для нахождения кол-ва строк матрицы
    int line; //Переменная, которая будет хранить строку с суммой ближайшей к нулю
    int Min = INT_MAX;
    int amount_J = 0; //Считает кол-во строк
    int total_J; //Хранит итоговое кол-во строк

    //Вводит числа в массив из файла, ищет ближайшую сумму строки к нулю и считает итоговое кол-во строк и столбцов.
    for (int i = 0; i < I; i++)
    {
        int Sum = 0;
        matr[i] = new int[J];

        for (int j = 0; j < J; j++)
        {
            fin >> n;
            Sum = Sum + n;
            matr[i][j] = n;
            Length_J = j;
        }
        if (amount_J == 0)
            total_J = Length_J + 1;
        else
            amount_J++;
        Sum = abs(Sum);
        if (Sum < Min)
        {
            Min = Sum;
            line = i;
        }
        Length_I = i + 1;
    }

    //вводит в файл данные из массива и из нужной строки ,ближайшей к нулю, выводит нули в файл и в массив.
    for (int i = 0; i < Length_I; i++)
    {
        for (int j = 0; j < total_J; j++)
        {
            fin >> n;
            if (i == line)
            {
                fout << "0" << "\t";
                matr[i][j] = 0;
            }
            else
            {
                fout << matr[i][j] << "\t";
            }
        }
        fout << "\n";
    }

    //Просто вывод в консоль итогового массива.
    for (int i = 0; i < Length_I; i++)
    {
        for (int j = 0; j < total_J; j++)
            cout << matr[i][j] << "  ";
        cout << endl;
    }

    for (int i = 0; i < I; i++)
        delete[] matr[i];
    delete[] matr;

    fin.close();
    fout.close();

    return 0;
}